package com.example;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@EnableBinding(Sink.class)
@EnableDiscoveryClient
@SpringBootApplication
public class PersonServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonServiceApplication.class, args);
	}

	@MessageEndpoint
	class MessageReceiver{
		
		@Autowired
		private PersonRepository personRepository;
		
		@ServiceActivator(inputChannel = Sink.INPUT)
		public void acceptMessage(Object payload){
			System.out.println("-------------------------------------------------------------------------------");
			System.out.println(payload.toString());
			System.out.println("-------------------------------------------------------------------------------");
			
			//this.personRepository.save(new Person(s));
		}
	}
	
	@RefreshScope
	@RestController
	class MessageRestController{
		
		@Value("${message}")
		private String message;
		
		@RequestMapping("/message")
		public @ResponseBody String message(){
			return this.message;
		}
		
	}
}


interface PersonRepository extends MongoRepository<Person, String>{
	public Person findByName(String name);
}

@RestController
class PersonController{
	
	@Autowired
	private PersonRepository personRepository;
	
	@RequestMapping("/persons")
	public List<Person> getPersons(){
		return this.personRepository.findAll();
	}
}

@Document(collection = "Person")
class Person implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Person() {
		super();
	}
	
	public Person(String s){
		this.name = s;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Person={");
		sb.append("id=").append(id);
		sb.append(", name=").append(name);
		sb.append("}");
		return sb.toString();
	}
}

